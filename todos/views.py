from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm
# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {"todo_list_detail": detail}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    form = TodoListForm()
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_lists = form.save()
            return redirect('todo_list_detail', id=todo_lists.id)

    else:
        form = TodoListForm()

    context = {
        "form": form
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    edit = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edit)
        if form.is_valid():
            form = form.save()

            return redirect('todo_list_detail', id=id)

    else:
        form = TodoListForm(instance=edit)

    context = {
        "edit": edit,
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    name = TodoList.objects.get(id=id)
    if request.method == "POST":
        name.delete()
        return redirect("todo_list_list")
    return render(request, "todos/todo_list_delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)

    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, 'todos/todo_item_create.html', context)


def todo_item_update(request, id):
    task = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=task)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)

    else:
        form = TodoItemForm(instance=task)
    context = {
        "task": task,
        "form": form,
    }
    return render(request, "todos/todo_item_update.html", context)
